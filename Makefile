# This top-level makefile was created to ease Debian packaging.
# It must provide targets: 'clean', 'build', 'install' and 'check'.
# The 'install' target must install files under $(DESTDIR).
# Making 'build' the default target is nice to have.

.PHONY: clean \
        build \
        install \
        check

export CROSS_COMPILE :=

DIR         := $(PWD)/calibration
BINDIR      := $(DESTDIR)/usr/bin

EXTRA_CFLAGS += -DCONFIG_LIBNL20

build:
	$(MAKE) -C $(DIR) CFLAGS="$(CFLAGS) $(EXTRA_CFLAGS)" NFSROOT="/usr"

# Clean.

clean: 
	$(MAKE) -C $(DIR) clean

install:
	@set -e; \
	mkdir -p "$(BINDIR)"; \
	install -m 755 calibration/calibrator $(BINDIR);

#check: TODO!
